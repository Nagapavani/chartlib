function lineChart(mainOptions)
{
    var actualOptions = jQuery.extend(true, {}, mainOptions);
   function linechartResposive(options)
   {
    if (options)
    {
        options.container = options.container ? options.container : "body"

        options.height = options.height ? options.height : 300
        options.marginTop = options.marginTop ? options.marginTop : 30
        options.marginBottom = options.marginBottom ? options.marginBottom : 30
        options.marginRight = options.marginRight ? options.marginRight : 20
        options.marginLeft = options.marginLeft ? options.marginLeft : 50
        options.xParam = options.xParam ? options.xParam : $('#errormsg').html('Please check ... May be Data,x-Parameter or y-Parameter is missing.. ');
        options.yParam = options.yParam ? options.yParam : $('#errormsg').html('Please check ... May be Data,x-Parameter or y-Parameter is missing.. ');
        options.gridx = options.gridx ? options.gridx : false
        options.gridy = options.gridy ? options.gridy : false
        options.randomIdString = Math.floor(Math.random() * 10000000000);
        options.header = options.header ? options.header : "LINE CHART";
        options.headerOptions = options.headerOptions == false ? options.headerOptions : true;
        options.tooltip_circle = options.tooltip_circle ==false ? options.tooltip_circle :true;
        options.on_hover_tool_tip= options.on_hover_tool_tip ==false?options.on_hover_tool_tip:true;
        options.tooltip_text_font=options.tooltip_text_font?options.tooltip_text_font:'10px';
        options.x_axis_json_key_value =options.x_axis_json_key_value?options.x_axis_json_key_value:"year";
        options.y_axis_json_key_value =options.y_axis_json_key_value?options.y_axis_json_key_value:"value";
        if(options.tooltip_circle == true)
        {
        options.circle_color = options.circle_color ? options.circle_color: "#6c7e88";
        }
        else
        {
        options.circle_color = options.circle_color ? options.circle_color: 'none' ;
        }
        options.data_line_color =options.data_line_color? options.data_line_color : "#6c7e88";
        options.x_axis_line_color=options.x_axis_line_color ? options.x_axis_line_color:"#6c7e88";
        options.y_axis_line_color=options.y_axis_line_color ? options.y_axis_line_color:"#6c7e88";
        options.x_axis_opacity =options.x_axis_opacity? options.x_axis_opacity:1;
        options.y_axis_opacity =options.y_axis_opacity? options.x_axis_opacity:1;
        options.x_axis_lable_color =options.x_axis_lable_color?options.x_axis_lable_color:"#6c7e88";
        options.y_axis_lable_color = options.y_axis_lable_color?options.y_axis_lable_color:"#6c7e88";
        options.x_axis_label_font_size = options.x_axis_label_font_size? options.x_axis_label_font_size:"10px";
        options.y_axis_label_font_size = options.y_axis_label_font_size ? options.y_axis_label_font_size:"10px";
        options.x_axis_label_rotation = options.x_axis_label_rotation ? options.x_axis_label_rotation : -25;
        options.y_axis_label_rotation= options.y_axis_label_rotation ? options.y_axis_label_rotation : -25;
        options.graph_box_background_color = options.graph_box_background_color?options.graph_box_background_color:"#ffffff";
        options.header_text_color = options.header_text_color ?options.header_text_color:"#ffffff";
        options.header_back_ground_color=options.header_back_ground_color?options.header_back_ground_color:"#ffffff";
        options.data_line_show = options.data_line_show == false? options.data_line_show:true;
        options.tooltip_data_bgcolor = options.tooltip_data_bgcolor ?options.tooltip_data_bgcolor: '#ffffff';
        options.x_axis_label = options.x_axis_label == false ? options.x_axis_label = 'none':  options.x_axis_label_color;
        options.x_axis_tick = options.x_axis_tick == false ? options.x_axis_tick = 'none':  options.x_axis_tick_color;
        options.x_axis_line = options.x_axis_line == false ? options.x_axis_line = 'none': options.x_axis_line_color;
         options.y_axis_label = options.y_axis_label == false ? options.y_axis_label = 'none': options.y_axis_label_color;
        options.y_axis_tick = options.y_axis_tick == false ? options.y_axis_tick = 'none':  options.y_axis_tick_color;
        options.y_axis_line = options.y_axis_line == false ? options.y_axis_line = 'none': options.y_axis_line_color;   
    }
 $(options.container).empty();
    var actualOptioins = jQuery.extend(true, {}, options);
    var  data_line_color =options.data_line_color;
    var on_hover_tool_tip =options.on_hover_tool_tip;
    var data = options.data;
    var actualOptioins = jQuery.extend(true, {}, options);
    var header = options.header;
    var tooltip_text_font=options.tooltip_text_font;
    var containerid = options.container;
    var  tooltip_data_bgcolor=options.tooltip_data_bgcolor;
    var randomSubstring = options.randomIdString    
    // var chartContainerdiv = '<div class="chartContainer"  align="center" style="width: 80%; margin: auto; margin-top: 30px; font-size: 14px;font-style: inherit;"> <div class="graphBox" style="margin: auto; background-color: #374c59; width: 100%;left: 0px;top: 0px;overflow: hidden;position: relative;"> <div class="headerDiv" style="font-weight:bold;background-color: #425661;text-align: left;color: #239185; border-bottom: 1px solid rgba(192, 192, 192, 0.47);width: 100%; line-height: 2.5;font-size: 16px ;padding-left: 5px;">' + header + '</div><div id="line_chart_div' + randomSubstring + '" class="chartContentDiv" style="width: 100%;"></div> </div></div>'
    var circle_color=  options.circle_color;
    var h1 = options.height;
    var h = parseInt(h1) + 50;
    var x_axis_line_color=options.x_axis_line_color;
    var y_axis_line_color=options.y_axis_line_color;
    var x_axis_opacity = options.x_axis_opacity;
    var y_axis_opacity = options.y_axis_opacity;
    var x_axis_label_font_size=options.x_axis_label_font_size;
    var y_axis_label_font_size= options.y_axis_label_font_size;
    var _this = options; 
    var modalwidth = $(window).width() - 200;
    var modal = ' <div id="modal_' + randomSubstring + '"class="modal fade " tabindex="-1" role="dialog"> <div class="modal-dialog modal-lg" style="width:' + modalwidth + 'px"> <form ><div class="modal-content"><div class="modal-body"  style="padding:0;background-color:#334d59" id="modal_chart_container' + randomSubstring + '"></div></div></form></div></div>';
    var graph_box_background_color = options.graph_box_background_color;
    var header_text_color= options.header_text_color;
    var header_back_ground_color=options.header_back_ground_color;
    var chartContainerdiv = '<div class="chartContainer"  align="center" style="width: 100%; margin: auto; margin-top: 0px; font-size: 14px;font-style: inherit;"> <div class="graphBox" style="height:' + h + 'px;max-height: ' + h + 'px;min-height: ' + h + 'px;margin: auto; background-color: '+ graph_box_background_color +'; width: 100%;left: 0px;top: 0px;position: relative;"> <div class="headerDiv" style="font-weight:bold;background-color:'+header_back_ground_color+' ; text-align: left;color:'+ header_text_color +' ; border-bottom: 1px solid rgba(192, 192, 192, 0.47);width: 100%; line-height: 2.5;font-size: 16px ;padding-left: 5px;">' + header + '</div><div id="line_chart_div' + randomSubstring + '" class="chartContentDiv" style="width: 100%;"></div> </div></div>'
    $(options.container).html(chartContainerdiv);
    $(options.container).append(modal);
    var chart_container;
    var data_line_show = options.data_line_show;
    var x_axis_label_rotation = options.x_axis_label_rotation;
    var y_axis_label_rotation = options.y_axis_label_rotation;
    var chart_container = "#line_chart_div" + randomSubstring;
    var x_axis_lable_color =options.x_axis_lable_color;
    var y_axis_lable_color =options.y_axis_lable_color;
    options.width = options.width ? options.width : $(chart_container).width() - 10;
    
    var  w= options.width;
    console.log("width"+w);
    if (!options.headerOptions) {
        var closebtn = '<button type="button" class="cancel" style="float: right;padding:0 8px;border:0;opacity: 1;background-color: transparent;float:right" data-dismiss="modal" aria-label="Close"> <span class="fa fa-remove"></span></button>'
        $(chart_container).siblings(".headerDiv").append(closebtn);
    }

    if ($(chart_container).siblings(".headerDiv").find(".bstheme_menu_button").length == 0)
        var header_options = '<div style="float: right; padding: 0 10px; cursor: pointer;" class="bstheme_menu_button bstheme_menu_button_' + randomSubstring + '" data-toggle="collapse" data-target="#opt_' + randomSubstring + '"><i class="fa fa-ellipsis-v" aria-hidden="true"></i><div class="bstheme_options" style="position:absolute;right:10px;z-index:2000"> <div style="display:none; min-width: 120px; margin-top: 2px; background: rgb(27, 39, 53) none repeat scroll 0% 0%; border: 1px solid rgb(51, 51, 51); border-radius: 4px;" id="opt_' + randomSubstring + '" class="collapse"><span style="display: inline-block;float: left;text-align: center;width: 33.33%; border-right: 1px solid #000;" class="header_fullscreen_chart' + randomSubstring + '"><i class="fa fa-expand" aria-hidden="true"></i></span><span style="display: inline-block;float: left;text-align: center;width: 33.33%; border-right: 1px solid #000;" class="header_refresh_' + randomSubstring + '"><i class="fa fa-refresh" aria-hidden="true"></i></span> <span style="display: inline-block;float: left;text-align: center;width: 33.33%;" class="header_table_' + randomSubstring + '"> <i class="fa fa-table" aria-hidden="true"></i></span> <span style="display: none;float: left;text-align: center;width: 33.33%;" class="header_chart_' + randomSubstring + '" ><i class="fa fa-bar-chart" aria-hidden="true"></i></span></div></div> </div>';
    $(chart_container).siblings(".headerDiv").append(header_options);

    if (!options.headerOptions) {
        $(chart_container).siblings(".headerDiv").find(".bstheme_options").css("right", "28px");
        $('.header_fullscreen_chart' + randomSubstring).css("display", "none");
    } else {
        $(chart_container).siblings(".headerDiv").find(".bstheme_options").css("right", "0");
    }
    var tool_tip = $('body').append('<div class="line_Chart_tooltip" style="position: absolute; opacity: 1; pointer-events: none; visibility: hidden;background-color:'+ tooltip_data_bgcolor +';padding: 10px;border-radius: 5px;border: 1px solid gray;font-size:'+ tooltip_text_font +';color:black;"><span style=" font-size: 12px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');

    var colorScale = d3.scaleOrdinal().range(["#46a2de", "#5888C8", "#31d99c", "#de5942", "#ffa618"]);
    var svg = d3.select(chart_container).append("svg").attr("width", options.width).attr("height", options.height),
            margin = {top: options.marginTop, right: options.marginRight, bottom: options.marginBottom, left: options.marginLeft},
    width = options.width - margin.left - margin.right,
            height = options.height - margin.top - margin.bottom;


    var parseTime = d3.timeParse("%Y");
    bisectDate = d3.bisector(function (d) {
        return x_axis_json_key_value;
    });

    var x = d3.scaleTime().range([0, width]);
    var y = d3.scaleLinear().range([height, 0]);


 var x_axis_json_key_value = options.x_axis_json_key_value;
    var y_axis_json_key_value = options.y_axis_json_key_value;



    function make_x_axis() {
        return d3.axisBottom(x)

        //    .ticks(8)

    }


    function make_y_axis() {
        return d3.axisLeft(y)
        //  .ticks(8);
    }
    
   // var a='d.year';
   console.log('val'+x_axis_json_key_value);
 

    
   var line = d3.line()


            .x(function (d) {     
               console.log('val2'+(x_axis_json_key_value));
 
                return x(d[options.x_axis_json_key_value]); // x-axis dynamic key value
            //   return x(x_axis_json_key_value);
            })
            .y(function (d) {
                return y(d[options.y_axis_json_key_value]); // y-axis dynamic key value
                //return y(y_axis_json_key_value);
            });

        var valueline2 = d3.line()
        .x(function(d) {  

            console.log("x-axis "+d[options.x_axis_json_key_value]);
         return x(d[options.x_axis_json_key_value]); 
            })

        .y(function(d) { 
       
        console.log("value2"+d.value2);
        return y(d.value2); 

          });


            console.log("line"+line)
   
    console.log('x_axis_json_key_value'+x_axis_json_key_value);
    console.log('y_axis_json_key_value'+y_axis_json_key_value);

    var g = svg.append("g")
            .attr("transform", "translate(" + margin.left + "," + (margin.top) + ")");


    var linedata = options.data;

    if (linedata) {
        if (linedata.length)
            drawline(linedata);
    } else {
        console.error("Data Handling Error : No data To plot the graph");
    }

    function drawline(data)
    {


        x.domain(d3.extent(data, function (d) {
            return d[options.x_axis_json_key_value];
           // return x_axis_json_key_value;
        }));
        y.domain([d3.min(data, function (d) {
                return d[options.y_axis_json_key_value];

              // return y_axis_json_key_value;
            }) , d3.max(data, function (d) {
              return Math.max(d[options.y_axis_json_key_value],d.value2);
              //  return y_axis_json_key_value;
            }) ]);


        //   .text("Population)");

        if (options.gridx == true)
        {

            var x_g = g.append("g")
                    .attr("class", "bs_linegrid")
                    .attr("transform", "translate(30," + (height) + ")")
                    //  .attr("class","gridx").style("fill","red")
                    .call(make_x_axis()
                            .tickSize(-height, 0, 0)
                            .tickFormat("")
                            );
            x_g.selectAll("path").style("stroke", "#6c7e88")
                    .style("shape-rendering", "crispEdges")
                    .style("fill", "none");
            x_g.selectAll("line").style("stroke", "#6c7e88")// grid y line color
                    .style("shape-rendering", "crispEdges")
                    .style("fill", "none");
           x_g.selectAll("text").style("fill", x_axis_lable_color)
                .style("font-size", x_axis_label_font_size)
                .style("stroke", "none")
              //  .attr("transform", "translate(100," + (height) + ")")
                .attr("transform", "rotate("+x_axis_label_rotation+")");  


        }
        if (options.gridy == true)
        {

            var y_g = g.append("g")
                    .attr("class", "bs_linegrid")
                    .call(make_y_axis()
                            .tickSize(-width, 0, 0)
                            .tickFormat("")
                            );

            y_g.selectAll("text").style("fill", "#6c7e88")
                    .style("font-size", "10px")
                    .style("stroke", "none");

            y_g.selectAll("path").style("stroke", "#6c7e88")
                    .style("shape-rendering", "crispEdges")
                    .style("fill", "none");
                    
            y_g.selectAll("line").style("stroke", "#6c7e88")
                    .style("shape-rendering", "crispEdges")
                    .style("fill", "none")

        }

var x_axis_label =options.x_axis_label;
console.log("val"+x_axis_label);
var x_axis_line =options.x_axis_line;
var x_axis_tick = options.x_axis_tick;
var y_axis_label =options.y_axis_label;
console.log("val"+x_axis_label);
var y_axis_line =options.y_axis_line;
var y_axis_tick = options.y_axis_tick;
        var x_g = g.append("g")
                .attr("class", "#6c7e88")
                .style("stroke","none")
                .attr("transform", "translate(0," + (height) + ")")
                //   .attr("transform", "rotate(0)")
                .call(d3.axisBottom(x));


//axis line style
        x_g.selectAll("path").style("stroke",  x_axis_line)
                .style("opacity", x_axis_opacity)
                .style("shape-rendering", "crispEdges")
                .style("fill", "none");
                
//ticks style
        x_g.selectAll("line").style("stroke", x_axis_tick)
       //  x_g.selectAll("line").style("stroke", "none")
                .style("shape-rendering", "crispEdges")
                .style("fill", "none");
   
        x_g.selectAll("text").style("fill", x_axis_label)
                .style("font-size", x_axis_label_font_size)
                .style("stroke", "none")
              //  .attr("transform", "translate(100," + (height) + ")")
                .attr("transform", "rotate("+x_axis_label_rotation+")");                
 
        var y_g = g.append("g")
                .attr("class", "bs_axisY").style("stroke", "#6C7E88")
                .call(d3.axisLeft(y).tickFormat(function (d) {
                    return parseInt(d / 1000) + "k";
                }));

     y_g.append("text")
                .attr("class", "axis-title")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                //.attr("dy", ".71em")
                .style("text-anchor", "end")
                .attr("fill", "#5D6971")
                .style("font-size", "20px")

//text rotation for y axis 
        y_g.selectAll("text").style("fill",y_axis_label)
                .style("font-size", y_axis_label_font_size)
                .style("stroke", "none")
                .attr("transform", "rotate("+y_axis_label_rotation+")"); 


        y_g.selectAll("path").style("stroke", y_axis_line)
                .style("opacity", y_axis_opacity)
                .style("shape-rendering", "crispEdges")
                .style("fill", "none");
// ticks for y axis 
        y_g.selectAll("line").style("stroke",y_axis_tick )
                .style("shape-rendering", "crispEdges")
                .style("fill", "none");

                

//line is creating from hear
if(data_line_show== true)
    {
        path = g.append("path")
                .data([data])
                .attr("class", "bs_line")
                .style("fill", "none")
                .style("stroke",data_line_color)
                .style("shape-rendering", "crispEdges")
                .attr("d", line)
                .transition()
                .duration(400)
                .ease(d3.easeLinear);

                  g.append("path")
                  .data([data])
                  .attr("class", "bs_line")
                   .style("fill", "none")
                  .style("stroke", "red")
                  .attr("d", valueline2)
                  .style("shape-rendering", "crispEdges")
                  .transition()
                  .duration(500)
                  .ease(d3.easeLinear);
     }
        var focus = g.append("g")
                .attr("class", "focus")
                .style("display", "none");

        focus.append("line")
                .attr("class", "x-hover-line hover-line")
                .attr("y1", 0)
                .attr("y2", height);

        focus.append("line")
                .attr("class", "y-hover-line hover-line")
                .attr("x1", width)
                .attr("x2", width);

        focus.append("circle")
                .attr("class", "bs_circle").style("fill", "#29B0B3").style("stroke", "#E8EEEF").style("stroke-width", "2px")

                .attr("r", 3.5); 


// chart tooltip starts
/*
var city = svg.selectAll(".city")
      .data(data)
      .enter().append("g")
      .attr("class", "city");

    city.append("path")
      .attr("class", "line")
      .attr("d", function(d) {
        return line(d.year);
      });*/
   /*   .style("stroke", function(d) {
        return color(d.value);
      })*/
/*
    city.append("text")
      .datum(function(d) {
        return {
          name: d.year,
          value:d.value
        };
      })
      .attr("transform", function(d) {
        return "translate(" + x(d.year) + "," + y(d.value) + ")";
      })
      .attr("x", 3)
      .attr("dy", ".35em")
      .text(function(d) {
        return d.year;

      });



var mouseG = svg.append("g")
      .attr("class", "mouse-over-effects");

    mouseG.append("path") // this is the black vertical line to follow mouse
      .attr("class", "mouse-line")
      .style("stroke", "black")
      .style("stroke-width", "1px")
      .style("opacity", "0");
      
    var lines = document.getElementsByClassName('line');

    var mousePerLine = mouseG.selectAll('.mouse-per-line')
      .data(data)
      .enter()
      .append("g")
      .attr("class", "mouse-per-line");

    mousePerLine.append("circle")
      .attr("r", 7)
     // .style("stroke", function(d) {
       // return color(d.year);
      //})
      .style("fill", "none")
      .style("stroke-width", "1px")
      .style("opacity", "0");

    mousePerLine.append("text")
      .attr("transform", "translate(10,3)");

    mouseG.append('svg:rect') // append a rect to catch mouse movements on canvas
      .attr('width', width) // can't catch mouse events on a g element
      .attr('height', height)
      .attr('fill', 'none')
      .attr('pointer-events', 'all')
      .on('mouseout', function() { // on mouse out hide line, circles and text
        d3.select(".mouse-line")
          .style("opacity", "0");
        d3.selectAll(".mouse-per-line circle")
          .style("opacity", "0");
        d3.selectAll(".mouse-per-line text")
          .style("opacity", "0");
      })
      .on('mouseover', function() { // on mouse in show line, circles and text
        d3.select(".mouse-line")
          .style("opacity", "1");
        d3.selectAll(".mouse-per-line circle")
          .style("opacity", "1");
        d3.selectAll(".mouse-per-line text")
          .style("opacity", "1");
      })
      .on('mousemove', function() { // mouse moving over canvas
        var mouse = d3.mouse(this);
        d3.select(".mouse-line")
          .attr("d", function() {
            var d = "M" + mouse[0] + "," + height;
            d += " " + mouse[0] + "," + 0;
            return d;
          });

        d3.selectAll(".mouse-per-line")
          .attr("transform", function(d, i) {
            console.log(width/mouse[0])
            var xDate = x.invert(mouse[0]),
                bisect = d3.bisector(function(d) { return d.year; }).right;
                idx = bisect(d.year, xDate);
            
            var beginning = 0,
                end = lines[i].getTotalLength(),
                target = null;

            while (true){
              target = Math.floor((beginning + end) / 2);
              pos = lines[i].getPointAtLength(target);
              if ((target === end || target === beginning) && pos.x !== mouse[0]) {
                  break;
              }
              if (pos.x > mouse[0])      end = target;
              else if (pos.x < mouse[0]) beginning = target;
              else break; //position found
            }
            
            d3.select(this).select('text')
              .text(y.invert(pos.y).toFixed(2));
              
            return "translate(" + (mouse[0]) + "," + (pos.y+70) +")";
          });
      });*/

//chart tooltip ends

 if(on_hover_tool_tip==true)
 {
        g.selectAll("circle")
                .data(data)

                .enter()
                .insert("circle")
                .attr("fill", circle_color) //circle colors
                .style("opacity", "0.4")
                .style("pointer-events", "all")
                .on("mouseover", function (d) {
                    $(this).css("opacity", 1);
                    var valueStr = '';
                    if (d[options.y_axis_json_key_value]) {
                        valueStr = '<br><span><span style="color:#000;">Value: </span>' + d[options.y_axis_json_key_value] + '</span>'
                        valueStr2 = '<br><span><span style="color:#000;">Value: </span>' + d.value2 + '</span>'
                    }
                   
                    
                    $(".line_Chart_tooltip").html('<span><span style="color:#000;"> YEAR: </span>' + d[options.x_axis_json_key_value] + '</span>'  + valueStr+ valueStr2);
                    return $(".line_Chart_tooltip").css("visibility", "visible");
                    
                })
                .on("mousemove", function () {
                    $(".line_Chart_tooltip").css("top", (d3.event.pageY - 10) + "px")
                    return  $(".line_Chart_tooltip").css("left", (d3.event.pageX + 10) + "px");
                  
                })
                .on("mouseout", function () {
                    $(this).css("opacity", 0.4);
                    //hide tool-tip
                    return $(".line_Chart_tooltip").css("visibility", "hidden");
                })
                //color  gor

                .attr("cx", function (d) {
                  //  return x((x_axis_json_key_value));
                  return x((d[options.x_axis_json_key_value]));
                      })
                .attr("r", function (d) {
                    return 5;
                })

                .attr("cy", function (d) {
                    return y((d[options.y_axis_json_key_value]));
                })
         }
}



    }

  linechartResposive(jQuery.extend(true, {}, mainOptions));
$(window).on("resize", function () {
 setTimeout(function(){  
     linechartResposive(jQuery.extend(true, {}, mainOptions));
 },200);
});
     


}
