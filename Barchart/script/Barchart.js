 
function simpleVerticalBarChart(options){
  if(options)
  {
     options.container=options.container?options.container:"body"
     options.width=options.width?options.width:$(options.container).width()-10
     options.height=options.height?options.height:300
     options.header_font_size=options.header_font_size?options.header_font_size:20
     options.header_underline=options.header_underline?options.header_underline:"underline"
     options.header_color=options.header_color?options.header_color:"black"
     options.marginTop=options.marginTop?options.marginTop:35
     options.marginBottom=options.marginBottom?options.marginBottom:70
     options.marginRight=options.marginRight?options.marginRight:30
     options.marginLeft=options.marginLeft?options.marginLeft:90
     options.xParam=options.xParam?options.xParam:$('#errormsg').html('Please check ... May be Data,x-Parameter or y-Parameter is missing.. ');
     options.yParam=options.yParam?options.yParam:$('#errormsg').html('Please check ... May be Data,x-Parameter or y-Parameter is missing.. ');
     options.gridx=options.gridx?options.gridx:false
     options.gridy=options.gridy?options.gridy:false
     options.yticks=options.yticks?options.yticks:10
     options.xticks=options.xticks?options.xticks:3
     
     //options.data=options.uri?options.uri:[]
     options.barwidth=options.barwidth?options.barwidth:false
     options.padding=options.padding?options.padding:0.2
     options.ytickPadding=options.ytickPadding?options.ytickPadding:5
     options.xtickPadding=options.xtickPadding?options.xtickPadding:""
       //options.x_axis_line = options.x_axis_line == false ? options.x_axis_line = 'none': options.x_axis_line_color;
     options.x_axis_line=options.x_axis_line?options.x_axis_line:"none"
     options.y_axis_line=options.y_axis_line?options.y_axis_line:"none"
     options.x_axis_ticks_show=options.x_axis_ticks_show?options.x_axis_ticks_show:"none"
     options.x_axis_ticks_color=options.x_axis_ticks_color?options.x_axis_ticks_color:"black"
     
     options.y_axis_ticks_show=options.y_axis_ticks_show?options.y_axis_ticks_show:"none"
     options.y_axis_ticks_color=options.y_axis_ticks_color?options.y_axis_ticks_color:"black"
     options.xText=(options.xText==false)?options.xText:"Letter"
     options.xText_font_size=options.xText_font_size?options.xText_font_size:18
     options.xText_font_color=options.xText.font_color?options.xText_font_color:"purple"
     options.yText=(options.yText==false)?options.yText:"Frequency"
     
     options.yText_font_size=options.yText_font_size?options.yText_font_size:18
     options.yText_color=options.yText_color?options.yText_color:"#b4b4b4"
     options.header=options.header?options.header:"BAR CHART"
     options.colors=options.colors?options.colors:"blue"
     options.x_axis_label_rotation = options.x_axis_label_rotation ? options.x_axis_label_rotation : -5;
     options.x_axis_label_font_size= options.x_axis_label_font_size?options.x_axis_label_font_size:15;         
     options.x_axis_line_color=options.x_axis_line_color?options.x_axis_line_color:"black"  
     options.x_axis_lable_color=options.x_axis_lable_color?options.x_axis_lable_color:"blue"
     options.x_axis_opacity=options.x_axis_opacity?options.x_axis_opacity:0.4
     options.y_axis_label_rotation= options.y_axis_label_rotation ? options.y_axis_label_rotation :-25;
     options.y_axis_lable_color= options.y_axis_lable_color? options.y_axis_lable_color:"black"
     options.y_axis_label_font_size=options.y_axis_label_font_size?options.y_axis_label_font_size:15
     options.y_axis_text_rotation=options.y_axis_text_rotation?options.y_axis_text_rotation:-90
     options.y_axis_line_color=options.y_axis_line_color?options.y_axis_line_color:"black"
     options.y_axis_opacity=options.y_axis_opacity?options.y_axis_opacity:0.2
     options.y_axis_grid_ticks=options.y_axis_grid_ticks?options.y_axis_grid_ticks:10
     options.tooltip=options.tooltip?options.tooltip:false
     options.tooltip_opacity=options.tooltip_opacity?options.tooltip_opacity:1
     options.tooltip_background_color=options.tooltip_background_color?options.tooltip_background_color:"#0cae96"
     options.tooltip_text=options.tooltip_text?options.tooltip_text:"uppercase"
     options.tootip_border_radius=options.tootip_border_radius?options.tootip_border_radius:"5px"
     options.tooltip_border=options.tooltip_border?options.tooltip_border:"1px solid gray"
     options.tooltip_font_size=options.tooltip_font_size?options.tooltip_font_size:"10px"
     options.tooltip_font_color=options.tooltip_font_color?options.tooltip_font_color:"black"
     //options.colorstol=options.colorstol?options.colorstol:["green","pink","red"]
     // options.barcolor_mouseover=options.barcolor_mouseover?options.barcolor_mouseover:"block"
      options.barwidthspace=options.barwidthspace?options.barwidthspace:""
      }  
    var tool_tip = $('body').append('<div class="Bar_Chart_tooltip" style="position: absolute;z-index: 99999;opacity:'+ options.tooltip_opacity +'; pointer-events: none; display:none;text-transform:'+options.tooltip_text+';background-color:'+options.tooltip_background_color+';  padding: 10px;border-radius: '+options.tootip_border_radius+'; border:'+options.tooltip_border+';font-size:'+options.tooltip_font_size+';color:'+options.tooltip_font_color+';"><span style=" font-size:40px; position: absolute; white-space: nowrap;  margin-left: 0px; margin-top: 0px; left: 8px; top: 8px;"><span style="font-size:10px" class="tool_tip_x_val"></span><table><tbody><tr><td style="padding:0"> </td><td style="padding:0"><b>216.4 mm</b></td></tr><tr><td style="color:#434348;padding:0">New York: </td><td style="padding:0"><b>91.2 mm</b></td></tr><tr><td style="color:#90ed7d;padding:0">London: </td><td style="padding:0"><b>52.4 mm</b></td></tr><tr><td style="color:#f7a35c;padding:0">Berlin: </td><td style="padding:0"><b>47.6 mm</b></td></tr></tbody></table></span></div>');
  
      
    var   margin = {top: options.marginTop, right:options.marginRight , bottom:options.marginBottom , left:options.marginLeft };
    var   width  =  options.width - margin.left - margin.right,
          height = options.height - margin.top - margin.bottom;
      
    var svg = d3.select(options.container).append("svg")
                .attr("width", width)
                .attr("height", height+margin.top + margin.bottom)


             //add the header
        svg.append("text")
           .attr("x", (width / 2))             
           .attr("y", 25)
           .attr("text-anchor", "middle")
           .style("fill", options.header_color)  
           .style("font-size", options.header_font_size) 
           .style("text-decoration", options.header_underline)  
           .text(options.header);
         
    var g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

     
    var x = d3.scaleBand().rangeRound([0,width]).padding(options.padding),
        y = d3.scaleLinear().rangeRound([height, 0]);
          
    var data = options.data
  
          
        data.forEach(function(d) {
            d.letter=d.letter,
            d.frequency = +d.frequency;
          });

       x.domain(data.map(function(d) { return d.letter; }));
       y.domain([0, d3.max(data, function(d) { return d.frequency; })]);
        
  
       g.append("g")
            //.attr("class", "axis axis--x stack")

        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x).tickPadding(options.xtickPadding))
       g.selectAll(".tick text")
        .style("fill", options.x_axis_lable_color)
        .style("font-size", options.x_axis_label_font_size)
        .style("stroke", "none") 
        //.attr("transform", "rotate("+options.x_axis_label_rotation+")")               
                               
        .call(wrap,options.barwidth?options.barwidth:x.bandwidth())
           // x_axis line style
       g.selectAll("path").style("stroke", options.x_axis_line_color)
        .style("opacity", options.x_axis_opacity)
        .style("shape-rendering", "crispEdges")
        .style("display",options.x_axis_line)
        .style("fill", "none");
      
      
          
     // ticks style
       g.selectAll("line")

        .style("stroke",options.x_axis_ticks_color)
        .style("display",options.x_axis_ticks_show)
        .style("shape-rendering", "crispEdges")

               // .attr("transform", "translate("+x.bandwidth()/data.length + ",0)")
        .style("fill", "none")
     //text style
     
                 //.attr("transform", "translate(70,0)")
               //.attr("transform", "rotate("+options.x_axis_label_rotation+")")               
                
      //.style("display",options.axisX)
       g.append("text")
        .attr("transform", "translate(" + (options.width /2) + "," +15+ ")")

        .attr("dx",0.02)
        .attr("y", 220)
                .attr("dx", "-10em")
        .style("text-anchor", "middle")
        .style("fill",options.xText_font_color)
        .text(options.xText).style("font-size", options.xText_font_size);
var g= g.append("g")
        .attr("class", "axis axis--y stack")
        .call(d3.axisLeft(y).ticks(options.yticks)
        .tickPadding(options.ytickPadding))
       g.selectAll("path").style("stroke", options.y_axis_line_color)
        .style("opacity", options.y_axis_opacity)
        .style("shape-rendering", "crispEdges")
        .style("display",options.y_axis_line)
        .style("fill", "none");
       g.selectAll("line").style("stroke", options.y_axis_ticks_color)
        .style("display",options.y_axis_ticks_show)
        .style("shape-rendering", "crispEdges")
        .style("fill", "none")
       g.selectAll("text").style("fill", options.y_axis_lable_color)
        .style("font-size", options.y_axis_label_font_size)
        .style("stroke", "none")
        .attr("transform", "rotate("+options.y_axis_label_rotation+")");
       g.append("text")
        .attr("transform", "rotate(-90)")
        .attr("y",-margin.left+20)
        .attr("dy", "1.1em")
        .attr("dx", -20)
        .style("text-anchor", "end")
        .style("fill",options.yText_color)
        .text(options.yText).style("font-size", options.yText_font_size);
                 
           function make_x_axis() {
              // return d3.axisBottom(x)
              //          .ticks(5)
              //    }
                return d3.axisBottom(x)
                         .ticks(5)
                    }

           function make_y_axis() {
              return d3.axisLeft(y)
                       .ticks(options.y_axis_grid_ticks);
                 }
  
   
    if (options.gridx == true)
        {

            var x_g = g.append("g")
                       .attr("class", "bs_linegrid")
                       .attr("transform", "translate(30," + (height) + ")")
                    //  .attr("class","gridx").style("fill","red")
                        .call(make_x_axis()
                        .tickSize(-height, 0, 0)
                        .tickFormat("")
                            );
                     x_g.selectAll("path").style("stroke", "#6c7e88")
                        .style("shape-rendering", "crispEdges")
                        .style("fill", "none");
                     x_g.selectAll("line").style("stroke", "#6c7e88")// grid y line color
                        .style("shape-rendering", "crispEdges")
                        .style("fill", "none");
                     x_g.selectAll("text").style("fill", "#6c79e88")
                        .style("font-size", "10px")
                        .style("stroke", "none");
        }
        if (options.gridy==true)
        {

            var y_g =  g.append("g")
                    //.attr("class", "bs_linegrid")
                        .call(make_y_axis()
                        .tickSize(-width, 0, 0)
                        .tickFormat("")
                            );
                     y_g.selectAll("text").style("fill", "#6c7e88")
                        .style("font-size", "10px")
                        .style("stroke", "none");
                     y_g.selectAll("path").style("stroke", "#6c7e88")
                        .style("shape-rendering", "crispEdges")
                        .style("fill", "none");
                     y_g.selectAll("line").style("stroke", "#6c7e88")
                        .style("shape-rendering", "crispEdges")
                        .style("fill", "none")
        }


    var g=g.append("g")
          g.selectAll(".bar")
           .data(data)
           .enter().append("rect")                      
           .attr("class", "bar")
           .attr("dy", ".35em") 
           .attr("x",function(d,i){
            console.log(x(d.letter));
                  if(options.barwidth==false)
                         {
                             console.log("hello");
                           // return x(d.letter);
                           return x(d.letter);
                         }
                    // else
                    // {
                    //   return (x(d.letter)+x.bandwidth()/data.length)-x.bandwidth()/15;
                    // }
                         else if(options.barwidth==10)
                                {
                                   return x(d.letter) +x.bandwidth()/data.length+46;
                                }
                         else if(options.barwidth==50)
                                {
                                   return x(d.letter) +x.bandwidth()/data.length+25;
                                }

                         else if(options.barwidth==100)
                                {
                                    console.log("hi");
                                    return x(d.letter) +x.bandwidth()/data.length;
                                }
                         else if(options.barwidth==150)
                                {
                                    return x(d.letter)+x.bandwidth()/data.length-23;
                                }  
                         else if(options.barwidth==200)
                                {
                                    return x(d.letter)+x.bandwidth()/data.length-50;
                                } 
                         else if(options.barwidth==250)
                                {
                                    return x(d.letter)+x.bandwidth()/data.length-70;
                                }
                         else if(options.barwidth==300)
                                {
                                    return x(d.letter)+x.bandwidth()/data.length-100
                                }
                         else if(options.barwidth==350)
                                {
                                    return x(d.letter)+x.bandwidth()/data.length-125;
                                }
                         })
               //.attr("x",function(d){return x(d.letter)+x.bandwidth()/2;})
          .attr("y", function(d) { return y(d.frequency); })
          .style("fill", function(d) {  
                    return options.colors;
                  })
          .attr("width",function(d) { 

                   if(options.barwidth == false){
                     return x.bandwidth();
                     //console.log(options.barwidth);
                   } else{
                     return options.barwidth;
                     
                   }
                  })

          .attr("height", function(d) { return height - y(d.frequency); })
          .on("mouseover", function (d) {
                 if(options.tooltip==false){
                     return false;
                  }  
          $(".Bar_Chart_tooltip").html('<span>' + d.letter + " : " + d.frequency+'</span>')
           d3.select(this)
             .style("fill", "brown");
              return $(".Bar_Chart_tooltip").css("display","block") 
            })
          .on("mousemove", function (d) {
                   if(options.tooltip==false){
                      return false;
                   }  
    var p=$(options.container)
      var position=p.offset();
      var windowWidth=window.innerWidth;
      var tooltipWidth=$(".Bar_Chart_tooltip").width()+50
      var cursor=d3.event.x;
      if((position.left<d3.event.pageX)&&(cursor>tooltipWidth)){
        var element = document.getElementsByClassName("Bar_Chart_tooltip");
        for(i=0;i<element.length;i++){
        element[i].classList.remove("tooltip-left");
      element[i].classList.add("tooltip-right");
        }
          $(".Bar_Chart_tooltip").css("left",(d3.event.pageX - 15-$(".Bar_Chart_tooltip").width()) + "px");
      }else{
        var element =document.getElementsByClassName("Bar_Chart_tooltip");
        for(i=0;i<element.length;i++){
          element[i].classList.remove("tooltip-right");
          element[i].classList.add("tooltip-left");
        }
          $(".Bar_Chart_tooltip").css("left", (d3.event.pageX + 10) + "px");
   
      }
        return $(".Bar_Chart_tooltip").css("top",d3.event.pageY + "px")
  })
  .on("mouseout", function (d) {
    d3.select(this) .style("fill", function(d) {  
            
              return options.colors;
            
        })

    //  $(".Bubble_Chart_tooltip").css("visibility", "hidden");
          return $(".Bar_Chart_tooltip").css("display", "none");
  })
}
//text breaking
function wrap(text, width) {
  console.log(width);
  text.each(function() {
     var text = d3.select(this),

        words = text.text().split(/\s+/).reverse(),

        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        y = text.attr("y"),
        dy = parseFloat(text.attr("dy")),

        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
      console.log(dy);
    while (word = words.pop()) {
      //console.log(word);
      line.push(word);
     // console.log(line);
      tspan.text(line.join(" "));
      //console.log(tspan);
      if (tspan.node().getComputedTextLength() > width) {
        console.log(line);
         line.pop();

        tspan.text(line.join(" "));
        line = [word];
       //console.log(line);
        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
      }
    }
  });
}
